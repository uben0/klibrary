# KLibrary

KLibrary is a set of modules providing classes and functions to easily create a
graphic program like a 2D game.


There are modules for windows (`KWindow`),
events (`KEvent`), images (`KSurface`), texts (`KFont` and `KText`), network
(`KSocket`), etc...


## Get started

Clone the git repository in your project folder and include in your code the
required headers from klibrary. Make sure the correct flags are set for
compilation. See `pkg-config --libs --clfags sdl2 SDL2_ttf SDL2_image`.

The documentation is available [here](http://klibrary.uben.ovh).


## Example

[Here](https://gitlab.com/uben0/pong) you will find a pong game using the
library. Have a look to the [Makefile](https://gitlab.com/uben0/pong/blob/master/Makefile).


```c++
#include <iostream>
#include "klibrary/KLibrary.hpp"

int main(int, const char*[]) {
    
    KWindow window("hello world", 1280, 720);
    KTimer timer(KTimer::Fps60);
    
    while (window.is_opened()) {
        
        window.fill(KColor::Black);
        window.display();
        timer.wait();
        
        window.event.update();
        if (window.event.close or window.event.quit) {
            window.close();
        }
        if (window.event.mouse.l) {
            std::cout << "mouse clic" << std::endl;
        }
    }
    return EXIT_SUCCESS;
}
```
